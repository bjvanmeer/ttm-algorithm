# TTM-algorithm

System Requirements
- All systems with an installation of R version 3.5.2 or higher. Previous versions of R might work but this is not tested. 
- For information on the system requirements of R please visit https://www.r-project.org/.

Installing and running the software (+/- 15 min)
1. install relevant libaries
2. run 'getMechanismScores' from the 'functions' file with 'rawConcentrationResponse' as input.
3. Save the resulting table 'probabilityScoreOverview'.
4. run 'getTheStats' from the 'functions' file with 'concentrationResponse' as input.
5. save the resulting table 'statOverview'
6. Run 'ui.R'

The tool should run instantly and be the same as the tool found here: https://bjvanmeer.shinyapps.io/TTM-algorithm/
Substituting the file 'ttm' with your own data file in the same format will make you able to view your own results.
