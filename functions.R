#libraries
library("SciViews")
library(data.table)
library(shiny)
library(ggplot2)
library(reshape2)
library(Rfast)
library(ggpubr)
library(RColorBrewer)
library(multcomp)
library(tidyverse)
library(formattable)
library(DT)
options(scipen=999)

plotTheGraphCR<-function(parameterName, datasetInput, input, parameterNames){
  #layout options general
  yStep<-0.075
  yAdd<-yStep
  pointSize<-2
  
  #define graph formatting
  graphLayers<-list(
    labs(y="Response \n[%, relative to baseline]"),
    theme_classic(base_size = 16),
    theme(plot.title = element_text(size=22, hjust = 0.5)),
    geom_hline(yintercept=100, linetype="dashed"),
    theme(plot.margin=unit(c(1,1,1.5,1.2),"cm"))
  )
  
  
  plotdf<-datasetInput[which(datasetInput$variable==parameterName),]
  plotdf<-plotdf[order(as.numeric(as.character(plotdf$concentration))),]
  if(input$statistics && dim(plotdf)[1] != 0){
    plotdf$concentration<-as.factor(plotdf$concentration)
    res.aov <- aov(value ~ concentration, data = plotdf)
    minConc<-min(as.numeric(as.character(unique(plotdf$concentration[which(plotdf$variable==parameterName)]))), na.rm=T)
    statLabels<-summary(glht(res.aov, linfct=mcp(concentration="Dunnett")))$test$pvalues
    statConcentrations<-names(summary(glht(res.aov, linfct=mcp(concentration="Dunnett"),base=minConc))$test$coefficients)
    statConcentrations<-as.numeric(str_remove(statConcentrations, paste(" - ",minConc,sep="")))
    for(l in (1:length(statLabels))){
      if(statLabels[l]<0.0001){
        statLabels[l]<-"****"
      }
      else if(statLabels[l]<0.001){
        statLabels[l]<-"***"
      }
      else if(statLabels[l]<0.01){
        statLabels[l]<-"**"
      }
      else if(statLabels[l]<0.05){
        statLabels[l]<-"*"
      } else {
        statLabels[l]<-""
      }
    }
    statText<-data.frame(statLabels, statConcentrations)
    colnames(statText)<-c("pValue","concentration")
    plotdf$concentration<-as.numeric(levels(plotdf$concentration))[plotdf$concentration]
  }
  
  
  if (dim(plotdf)[1] == 0) {
    ggplot(plotdf)+
      graphLayers+
      theme(axis.title.x=element_blank(),
            axis.text.x=element_blank(),
            axis.ticks.x=element_blank())+
      ylim(c(0,100))+
      annotate("text", x = 4, y = 50, label = "No data available. Please select other options.")
  } else {
    
    ggplot(plotdf, aes(x=concentration, y=value))+
      ggtitle(parameterNames[(which(parameterNames$parameterNamesRaw==parameterName)),"graphNames"])+
      graphLayers+
      theme(legend.position = "none")+
      scale_x_log10(name=expression(paste("Concentration [",mu,"M]",sep="")),breaks = sort(unique(plotdf$concentration)),
                    labels=c("Vehicle",sort(unique(plotdf$concentration))[2:length(unique(plotdf$concentration))]))+
                    { if(input$checkBoxPoint) geom_point(size=pointSize, aes(color=drug))  }+
                    { if(input$checkBoxLine) stat_summary(fun.y=mean, geom = "line", aes(group=drug, color=drug), size=1)  }+
                    { if(input$checkBoxAverage) stat_summary(fun.y=mean, geom = "errorbar", aes(ymax=..y.., ymin=..y.., group=drug, color=drug), size=1)  } +
                    { if(input$statistics) geom_text(vjust=0, data = statText, size=6, aes(x = concentration, y = (1+yStep)*max(plotdf$value, na.rm=T), label = statText$pValue, color = unique(plotdf$drug), group = unique(plotdf$drug)))}
  }
}


plotTheGraphCR_raw<-function(parameterName, datasetInput, input, parameterNames, quantity="relative"){
  #layout options general
  yStep<-0.075
  yAdd<-yStep
  pointSize<-2
  
  #define graph formatting
  graphLayers<-list(
    labs(y="Response \n[%, relative to baseline]"),
    theme_classic(base_size = 16),
    theme(plot.title = element_text(size=22, hjust = 0.5)),
    #geom_hline(yintercept=100, linetype="dashed"),
    theme(plot.margin=unit(c(1,1,1.5,1.2),"cm"))
  )
  
  if(quantity=="time"){
    graphLayers[[1]]$y<-"Time [milliseconds]"
  } else if(quantity=="contrAmplitude"){
    graphLayers[[1]]$y<-"Contractility [a.u.]"
  } else if(quantity=="dfAmplitude"){
    graphLayers[[1]]$y<-"Amplitude [a.u.]"
  } 
    
  
  
  plotdf<-datasetInput[which(datasetInput$variable==parameterName),]
  plotdf<-plotdf[order(as.numeric(as.character(plotdf$concentration))),]

  if (dim(plotdf)[1] == 0) {
    ggplot(plotdf)+
      graphLayers+
      theme(axis.title.x=element_blank(),
            axis.text.x=element_blank(),
            axis.ticks.x=element_blank())+
      ylim(c(0,100))+
      annotate("text", x = 4, y = 50, label = "No data available. Please select other options.")
  } else {
    
    
    ggplot(plotdf, aes(x=concentration, y=value, color=added))+
      ggtitle(parameterNames[(which(parameterNames$parameterNamesRaw==parameterName)),"graphNames"])+
      graphLayers+
      theme(legend.position = "top")+
      theme(legend.title = element_blank())+
      scale_x_log10(name=expression(paste("Concentration [",mu,"M]",sep="")),breaks = sort(unique(plotdf$concentration)),
                    labels=c("Vehicle",sort(unique(plotdf$concentration))[2:length(unique(plotdf$concentration))]))+
                    { if(input$checkBoxPointRaw) geom_point(size=pointSize, aes(color=added))  }+
                    { if(input$checkBoxLineRaw) stat_summary(fun.y=mean, geom = "line", aes(group=added, color=added), size=1)  }+
                    { if(input$checkBoxAverageRaw) stat_summary(fun.y=mean, geom = "errorbar", aes(ymax=..y.., ymin=..y.., group=added, color=added), size=1)  }
  }
}

getBaselineStats<-function(dataSet, parameterNames){
  statM<-cbind(setDT(dataSet)[,.(mean(value, na.rm=T)),'variable'], setDT(dataSet)[,.(sd(value, na.rm=T)),'variable'][,2])
  statM<-cbind(statM,Unit=c("a.u.","dF/F","milliseconds","milliseconds","milliseconds","milliseconds","milliseconds","dF/F","milliseconds","milliseconds"))
  colnames(statM)[2:3]<-c("Mean", "SD")
  statM[,2]<-format(round(statM[,2], 3), nsmall = 3)[,1]
  statM[,3]<-format(round(statM[,3], 3), nsmall = 3)[,1]
  for(i in 1:length(parameterNames$parameterNamesRaw)){
    statM[which(statM[,1]==as.character(parameterNames$parameterNamesRaw[i])),1]<-as.character(parameterNames$graphNames[i])
  }
  return(statM)
}

getTheStats<-function(concentrationResponse){
  #first we calculate normal stats:
  statOverview<-NULL
  for(drug in unique(concentrationResponse$drug)){
    print(drug)
    for(varOb in unique(concentrationResponse$variable)){
      plotdf<-concentrationResponse[which(concentrationResponse$variable==varOb & concentrationResponse$drug==drug),]
      plotdf<-plotdf[order(as.numeric(as.character(plotdf$concentration))),]
      plotdf$concentration<-as.factor(plotdf$concentration)
      res.aov <- aov(value ~ concentration, data = plotdf)
      minConc<-min(as.numeric(as.character(unique(plotdf$concentration[which(plotdf$variable==varOb)]))), na.rm=T)
      statLabels<-summary(glht(res.aov, linfct=mcp(concentration="Dunnett")))$test$pvalues
      statConcentrations<-names(summary(glht(res.aov, linfct=mcp(concentration="Dunnett"),base=minConc))$test$coefficients)
      statConcentrations<-as.numeric(str_remove(statConcentrations, paste(" - ",minConc,sep="")))
      statConcentrations<-c("Vehicle", statConcentrations)
      statText<-data.frame(statConcentrations, c("--", statLabels))
      for(l in (1:length(statLabels))){
        if(statLabels[l]<0.0001){
          statLabels[l]<-"p<0.0001"
        }
        else if(statLabels[l]<0.001){
          statLabels[l]<-"p<0.001"
        }
        else if(statLabels[l]<0.01){
          statLabels[l]<-"p<0.01"
        }
        else if(statLabels[l]<0.05){
          statLabels[l]<-"p<0.05"
        } else {
          statLabels[l]<-""
        }
      }
      statText<-cbind(statText, c("--", statLabels))
      colnames(statText)<-c("concentration", "pValue", "significance")
      for(i in as.numeric(levels(plotdf$concentration)[2:6])){
        if(!i %in% statText$concentration[-1]){
          levels(statText$concentration)<-c(levels(statText$concentration), i)
          statText<-rbind(statText, c(i, NA, NA))
        }
      }
      statText<-cbind(statText, varOb, drug, setDT(plotdf)[,.(mean(value, na.rm=T)),"concentration"][,-1], setDT(plotdf)[,.(sd(value, na.rm=T)),"concentration"][,-1])
      colnames(statText)[4]<-c("parameter")
      colnames(statText)[6:7]<-c("mean", "sd")
      statOverview<-rbind(statOverview,statText)
    }
  }
  return(statOverview)
}


getProbability<-function(hypothesis, value, vehicle, sd){

  #cumalative probability density plot
  fun<-function(x) 1/sqrt(2*pi*sd*sd)*exp(-((x-vehicle)^2)/(2*sd*sd))
  testMaxVal<-seq(0,(2*vehicle),by=0.01)

  maxProbD<-max(fun(testMaxVal))

  if(hypothesis==-11){
    hypothesisRange<-c(-1,1)
  } else if(hypothesis==-10){
    hypothesisRange<-c(-1,0)
  } else if(hypothesis==10){
    hypothesisRange<-c(0,1)
  } else {
    hypothesisRange<-hypothesis
  }

  probability<-numeric(0)
  for(hypothesis in hypothesisRange){
    for(i in 1:length(value)){
      if(is.na(value[i])){
        tempVal<-NA
      } else {
        tempVal<-fun(value[i])/maxProbD
        if(hypothesis==-1){
          if(value[i]<vehicle){
            tempVal<-1-tempVal
          } else {
            tempVal<-0
          }
        } else if(hypothesis==1){
          if(value[i]>vehicle){
            tempVal<-1-tempVal
          } else {
            tempVal<-0
          }
        }
      }
      if(is.na(probability[i])){
        probability[i]<-tempVal
      } else {
        probability[i]<-probability[i]+tempVal
      }
    }
  }
  probability<-probability/length(hypothesisRange)
  return(probability)
}





getMechanismScores<-function(rawConcentrationResponse){
  #get vehicle data
  meanVehicleOverview<-NULL
  setDT(rawConcentrationResponse)
  A<-rawConcentrationResponse[,.(min(concentration)),"drug"]
  colnames(A)<-c("drug", "concentration")
  A$minCon<-paste(A$drug,A$concentration,sep=":")
  C<-rawConcentrationResponse
  C$minCon<-paste(C$drug,C$concentration,sep=":")
  C<-C[minCon %chin% A$minCon]
  C$well<-substr(C$ID,0,5)
  
  meanVehicleOverview<-C[,.(mean(value, na.rm=T), sd(value, na.rm=T)),c("drug", "concentration","well", "variable")]
  meanVehicleOverview<-meanVehicleOverview[,.(mean(V1, na.rm=T), sd(V1, na.rm=T)),c("drug", "concentration", "variable")]
  colnames(meanVehicleOverview)[4:5]<-c("mean", "sd")

  #attach vehicle means
  meanVehicleOverview$match<-paste(meanVehicleOverview$drug,meanVehicleOverview$variable,sep=":")
  rawConcentrationResponse$mean<-paste(rawConcentrationResponse$drug,rawConcentrationResponse$variable,sep=":")
  rawConcentrationResponse$sd<-paste(rawConcentrationResponse$drug,rawConcentrationResponse$variable,sep=":")
  rawConcentrationResponse$mean<-as.factor(rawConcentrationResponse$mean)
  rawConcentrationResponse$sd<-as.factor(rawConcentrationResponse$sd)
  levels(rawConcentrationResponse$mean)<-meanVehicleOverview[order(rank(match))]$mean
  levels(rawConcentrationResponse$sd)<-meanVehicleOverview[order(rank(match))]$sd
  
  
  
  #define list of hypostheses
  investigateForIndex<-c("contraction_amplitude", "CSD_amplitude", "contraction_TTP90", "contraction_RT90", "CSD_TTP90", "CSD_RT90", "VSD_RT90", "VSD_amplitude", "VSD_APD90.APD30", "VSD_TTP90")
  upcAMP<-c(1,1,-1,-1,-1,-1,-1,0,-1,-1)  
  upCa<-c(1,1,10,10,10,10,-11,0,-1,-1)
  upMyosin<-c(1,0,1,10,-10,-10,0,0,0,0) 
  downCa<-c(-1,-1,-1,-1,-1,-1,-1,-1,-1,10)
  downOther<-c(-1,-1,-10,-10,-10,-10,-10,-1,1,1)
  vehicleCheck<-c(0,0,0,0,0,0,0,0,0,0)
  indexTable<-data.frame(investigateForIndex,upcAMP, upCa, upMyosin, downCa, downOther, vehicleCheck)
  indexTable<-indexTable[order(rank(investigateForIndex)),]
  hypotheses<-colnames(indexTable)[-1]
  
  rawConcentrationResponse[,hypotheses]<-rawConcentrationResponse$variable
  
  levels(rawConcentrationResponse$upcAMP)<-indexTable$upcAMP
  levels(rawConcentrationResponse$upCa)<-indexTable$upCa
  levels(rawConcentrationResponse$upMyosin)<-indexTable$upMyosin
  levels(rawConcentrationResponse$downCa)<-indexTable$downCa
  levels(rawConcentrationResponse$downOther)<-indexTable$downOther
  levels(rawConcentrationResponse$vehicleCheck)<-indexTable$vehicleCheck

  
  for(hypothesis in 1:length(hypotheses)){
    rawConcentrationResponse[,7+hypothesis] <- apply(rawConcentrationResponse, 1, function(x) getProbability(as.numeric(x[7+hypothesis]),as.numeric(x[3]),as.numeric(x[6]),sd=as.numeric(x[7])))  
  }

  #first average per well
  rawConcentrationResponse$well<-substr(rawConcentrationResponse$ID,0,5)
  probabilityScorePerWell<-rawConcentrationResponse[,.(sum(upcAMP, na.rm=T), sum(upCa, na.rm=T),sum(upMyosin, na.rm=T),sum(downCa, na.rm=T),sum(downOther, na.rm=T),sum(vehicleCheck, na.rm=T)), c("ID", "well", "drug", "concentration")]
  colnames(probabilityScorePerWell)[5:10]<-hypotheses
  probabilityScoreOverview<-probabilityScorePerWell[,.(mean(upcAMP, na.rm=T), mean(upCa, na.rm=T), mean(upMyosin, na.rm=T), mean(downCa, na.rm=T), mean(downOther, na.rm=T), mean(vehicleCheck, na.rm=T)), c("well", "drug", "concentration")]
  colnames(probabilityScoreOverview)[4:9]<-hypotheses
  #probabilityScoreOverview[probabilityScoreOverview == 0] <- NA
  probabilityScoreOverview<-probabilityScoreOverview[,.(mean(upcAMP, na.rm=T), mean(upCa, na.rm=T), mean(upMyosin, na.rm=T), mean(downCa, na.rm=T), mean(downOther, na.rm=T), mean(vehicleCheck, na.rm=T)), c("drug", "concentration")]
  colnames(probabilityScoreOverview)[3:8]<-hypotheses

  return(probabilityScoreOverview)
  
}